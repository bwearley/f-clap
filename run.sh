#!/bin/bash

PROG=./test_fclap

SEPARATOR='*********************'

echo "${SEPARATOR} Print Version Info ${SEPARATOR}"
${PROG} -v
echo "${SEPARATOR} Print Help Information ${SEPARATOR}"

${PROG} -h

echo "${SEPARATOR} Test Run With Switches ${SEPARATOR}"

${PROG} bundle.dat schedule.dat --no-wakes --output -p --full-gamma=2.5 -t sometag -x 1 22 333