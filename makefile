PROG     = test_fclap
DIR1     = ./
DIR2     = ../
FC       = gfortran
FFLAGS   = -cpp -Ddebug -Wall -Wextra -Warray-temporaries -Wconversion -fimplicit-none -fbacktrace -ffree-line-length-0 -fcheck=all -ffpe-trap=zero,overflow,underflow -finit-real=nan -static-libgfortran

#FC       = ifort
#FFLAGS   = -fpp -Ddebug -fpe0 -traceback -assume nosource_include

#FFLAGS  = $(FFLAGS) -DWIN32 # Windows Native

INCLUDES = -I $(DIR1) -I $(DIR2)
LIBS     =
LDFLAGS  =

vpath %.f90 $(DIR1):$(DIR2) 
vpath %.f   $(DIR1):$(DIR2) 

OBJS = \
	terminal_tools_mod.o \
	fclap_settings_mod.o \
	argument_group_class.o \
	argument_class.o \
	fclap_mod.o \
	fclap_config_smod.o \
	test_argparse.o

all: $(PROG)

$(PROG) : $(OBJS)
	$(FC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

clean:
	rm -f \$(PROG) \$(OBJS) *.mod *.smod

neat:
	rm -f \$(OBJS) *.mod *.smod

.SUFFIXES:
.SUFFIXES: .f .f90 .o

.f.o:
	$(FC) $(FFLAGS) $(INCLUDES) -c $<

.f90.o:
	$(FC) $(FFLAGS) $(INCLUDES) -c $<
