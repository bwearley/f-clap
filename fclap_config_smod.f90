!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! fclap_config_smod
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines the F-CLAP arguments
!-----------------------------------------------------------------------

submodule (fclap_mod) fclap_config_smod
    implicit none

contains

    module procedure configure_fclap
        use fclap_mod
        implicit none

        integer :: tag_idx
        
        !-- Allocate
        allocate(arg_groups(max_num_groups))
        allocate(arguments(max_num_arguments))
    
        call add_argument(Argument(name='bundle',                &
                                   nargs='1',                    &
                                   help='Shipping bundle.dat file'))
    
        call add_argument(Argument(name='schedule',       &
                                   nargs='1',             &
                                   help='Schedule file'))
    
        !-- Main Group
        call add_argument_group(Argument_Group('Main'))
      
        !-- Options Group
        call add_argument_group(Argument_Group('Options',                          &
                                desc='Optional switches for added functionality'))
    
        call add_argument(Argument(name='tag',group=arg_groups(group_idx), &
                                   switch_short='-t',switch_long='--tag',  &
                                   help='Tag to append to output files',   &
                                   important=.true.), get_index=tag_idx)
    
        call add_argument(Argument(name='output',group=arg_groups(group_idx), &
                                   switch_short='-o',switch_long='--output',  &
                                   action='store_true',                       &
                                   help='Do output',                          &
                                   important=.true.))
    
        call add_argument(Argument(name='print',group=arg_groups(group_idx), &
                                   switch_short='-p',switch_long='--print',  &
                                   action='store_false',                     &
                                   help='Suppress printouts',                &
                                   important=.true.))
    
        call add_argument_group(Argument_Group('Extras'))                   
    
        call add_argument(Argument(name='extras',group=arg_groups(group_idx), &
                                   switch_short='-x',switch_long='--extras',  &
                                   nargs='*',                                 &
                                   help='Extra files to process',             &
                                   important=.true.))
    
        call add_argument(Argument(name='t2files',group=arg_groups(group_idx), &
                                   switch_short='-g',switch_long='--g2f',      &
                                   nargs='3',                                  &
                                   help='Files of type t2',                    &
                                   important=.true.))
    
        !-- Calculation Options
        call add_argument_group(Argument_Group('Calculation Options',               &
                                desc='Options that change calculation properties'))
    
        call add_argument(Argument(name='no-wakes',group=arg_groups(group_idx), &
                                   switch_long='--no-wakes',                    &
                                   help='Suppress wakes',                       &
                                   action='store_true',                         &
                                   important=.true.))
    
        call add_argument(Argument(name='bonus',group=arg_groups(group_idx), &
                                   switch_long='--bonus',                    &
                                   nargs='+',                                &
                                   help='Bonus files to process',            &
                                   important=.true.))
    
        call add_argument(Argument(name='full-gamma',group=arg_groups(group_idx), &
                                   switch_long='--full-gamma',                    &
                                   nargs='1',                                     &
                                   help='Full gamma',                             &
                                   important=.true.))

        call add_argument(Argument(name='partial-gamma',group=arg_groups(group_idx), &
                                   switch_long='--partial-gamma',                    &
                                   nargs='1',                                        &
                                   help='Partial gamma',                             &
                                   important=.false.))

    end procedure

end submodule