!-----------------------------------------------------------------------
! Fortran Command Line Argument Parser (f-clap)
!-----------------------------------------------------------------------
!
! MODULE:
! terminal_tools_mod
!
! AUTHOR:
! Brian Earley
!
! DESCRIPTION:
!> Defines special escape characters for terminal printing
!-----------------------------------------------------------------------

module terminal_tools_mod
    implicit none
    public

!-- Rich Terminal Special Characters
#ifndef BASIC_TERMINAL

    !-- Special
    character(len=*),parameter :: term_esc           = achar(27)           !< Terminal escape character (\033)
    character(len=*),parameter :: term_clear         = term_esc // '[2J'   !< Clear terminal
    
    !-- Formatting
    character(len=*),parameter :: term_end           = term_esc // '[0m'   !< Clear formatting
    character(len=*),parameter :: term_bold          = term_esc // '[1m'   !< Bold formatting
    character(len=*),parameter :: term_dim           = term_esc // '[2m'   !< Dim formatting
    character(len=*),parameter :: term_underline     = term_esc // '[4m'   !< Underlined formatting
    character(len=*),parameter :: term_blink         = term_esc // '[5m'   !< Blink

    !-- Font Colors
    character(len=*),parameter :: term_red           = term_esc // '[31m'  !< Red
    character(len=*),parameter :: term_green         = term_esc // '[32m'  !< Green
    character(len=*),parameter :: term_yellow        = term_esc // '[33m'  !< Yellow
    character(len=*),parameter :: term_blue          = term_esc // '[34m'  !< Blue
    character(len=*),parameter :: term_magenta       = term_esc // '[35m'  !< Magenta
    character(len=*),parameter :: term_cyan          = term_esc // '[36m'  !< Cyan
    character(len=*),parameter :: term_lt_gray       = term_esc // '[37m'  !< Light Gray
    character(len=*),parameter :: term_dark_gray     = term_esc // '[90m'  !< Dark Gray
    character(len=*),parameter :: term_lt_red        = term_esc // '[91m'  !< Light Red
    character(len=*),parameter :: term_lt_green      = term_esc // '[92m'  !< Light Green
    character(len=*),parameter :: term_lt_yellow     = term_esc // '[93m'  !< Light Yellow
    character(len=*),parameter :: term_lt_blue       = term_esc // '[94m'  !< Light Blue
    character(len=*),parameter :: term_lt_magenta    = term_esc // '[95m'  !< Light Magenta
    character(len=*),parameter :: term_lt_cyan       = term_esc // '[96m'  !< Light Cyan
    character(len=*),parameter :: term_white         = term_esc // '[97m'  !< White

    !-- Background Colors
    character(len=*),parameter :: term_red_bg        = term_esc // '[41m'  !< Red
    character(len=*),parameter :: term_green_bg      = term_esc // '[42m'  !< Green
    character(len=*),parameter :: term_yellow_bg     = term_esc // '[43m'  !< Yellow
    character(len=*),parameter :: term_blue_bg       = term_esc // '[44m'  !< Blue
    character(len=*),parameter :: term_magneta_bg    = term_esc // '[45m'  !< Magenta
    character(len=*),parameter :: term_cyan_bg       = term_esc // '[46m'  !< Cyan
    character(len=*),parameter :: term_lt_gray_bg    = term_esc // '[47m'  !< Light Gray
    character(len=*),parameter :: term_dark_gray_bg  = term_esc // '[100m' !< Dark Gray
    character(len=*),parameter :: term_lt_red_bg     = term_esc // '[101m' !< Light Red
    character(len=*),parameter :: term_lt_green_bg   = term_esc // '[102m' !< Light Green
    character(len=*),parameter :: term_lt_yellow_bg  = term_esc // '[103m' !< Light Yellow
    character(len=*),parameter :: term_lt_blue_bg    = term_esc // '[104m' !< Light Blue
    character(len=*),parameter :: term_lt_magenta_bg = term_esc // '[105m' !< Light Magenta
    character(len=*),parameter :: term_lt_cyan_bg    = term_esc // '[106m' !< Light Cyan
    character(len=*),parameter :: term_white_bg      = term_esc // '[107m' !< White

!-- Plain Terminal -- No Special Characters
#else
    character(len=*),parameter :: term_esc           = ''
    character(len=*),parameter :: term_clear         = ''
    character(len=*),parameter :: term_end           = ''
    character(len=*),parameter :: term_bold          = ''
    character(len=*),parameter :: term_dim           = ''
    character(len=*),parameter :: term_underline     = ''
    character(len=*),parameter :: term_blink         = ''
    character(len=*),parameter :: term_red           = ''
    character(len=*),parameter :: term_green         = ''
    character(len=*),parameter :: term_yellow        = ''
    character(len=*),parameter :: term_blue          = ''
    character(len=*),parameter :: term_magenta       = ''
    character(len=*),parameter :: term_cyan          = ''
    character(len=*),parameter :: term_lt_gray       = ''
    character(len=*),parameter :: term_dark_gray     = ''
    character(len=*),parameter :: term_lt_red        = ''
    character(len=*),parameter :: term_lt_green      = ''
    character(len=*),parameter :: term_lt_yellow     = ''
    character(len=*),parameter :: term_lt_blue       = ''
    character(len=*),parameter :: term_lt_magenta    = ''
    character(len=*),parameter :: term_lt_cyan       = ''
    character(len=*),parameter :: term_white         = ''
    character(len=*),parameter :: term_red_bg        = ''
    character(len=*),parameter :: term_green_bg      = ''
    character(len=*),parameter :: term_yellow_bg     = ''
    character(len=*),parameter :: term_blue_bg       = ''
    character(len=*),parameter :: term_magneta_bg    = ''
    character(len=*),parameter :: term_cyan_bg       = ''
    character(len=*),parameter :: term_lt_gray_bg    = ''
    character(len=*),parameter :: term_dark_gray_bg  = ''
    character(len=*),parameter :: term_lt_red_bg     = ''
    character(len=*),parameter :: term_lt_green_bg   = ''
    character(len=*),parameter :: term_lt_yellow_bg  = ''
    character(len=*),parameter :: term_lt_blue_bg    = ''
    character(len=*),parameter :: term_lt_magenta_bg = ''
    character(len=*),parameter :: term_lt_cyan_bg    = ''
    character(len=*),parameter :: term_white_bg      = ''
#endif
end module