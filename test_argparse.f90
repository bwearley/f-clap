subroutine test_argparse
    use fclap_mod
    implicit none
    
    !-- Test Variables
    logical :: test_logical1
    real :: test_real1
    type(Argument) :: a
    character(len=:),allocatable :: t1
    character(len=mxclen),allocatable :: t2(:)

    ! Print Debug Info
    call print_debug_information

    !-- Additional Tests

    write (*,*) 'Additional Tests:'

    ! Copy Argument Data into a
    a = get_argument_named('full-gamma')

    ! Print whether `a' is configured
    print*, "Argument `a' is configured:", a % is_configured()

    ! Get value stored in full-gamma at command line (as string)
    t1 = trim(get_value_for_arg('full-gamma'))

    ! Test the output of t1
    print*,'Output stored in full-gamma: (',t1, ')'

    ! Read string t1 into a real variable
    if (len(trim(t1)) > 0) &
    read (t1,*) test_real1

    ! Print output as real variable
    print*,'Printout full-gamma as a real: ',test_real1

    ! Get multiple stored values from argument extras
    t2 = get_values_for_arg('extras')

    ! Print 1st stored argument in extras
    print*,'Extras, first argument: ',trim(t2(1))

    ! Print 3rd stored argument in extras
    print*,'Extras, third argument: ',trim(t2(3))

end subroutine

program main
    use fclap_mod
    implicit none

    ! Configure Test Setup
    call configure_fclap

    ! Parse CLI
    call parse_command_line_arguments

    ! Run Tests
    call test_argparse

end program main